env = "gcp"
kube_context= "rke2-cluster-0"
domain = "openaire.duckdns.org"
admin_user = "admin"
admin_password = "admin"
admin_hash = "$2y$10$Wd.mnnrDG01KJ42aVtC89.FdXOvyRm4RNfDfZ5F8k4r/fmSZgrIEq" # generate with htpasswd -bnBC 10 "" <admin_password>
s3_endpoint = "https://storage.googleapis.com"
s3_key= "google key"
s3_secret = "google secret"

# bucket skgif-openaire-eu
