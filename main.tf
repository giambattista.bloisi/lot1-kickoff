/*module "minio" {
  source = "./modules/minio"
  kube_context = "kind-openaire-data-platform"
}*/

module "opensearch-cluster" {
  source = "./modules/opensearch"
  kube_context = var.kube_context
  admin_user = var.admin_user
  admin_password = var.admin_password
  admin_hash = var.admin_hash
  env = var.env
  domain = var.domain
}

module "airflow" {
  source = "./modules/airflow"
  kube_context = var.kube_context
  admin_user = var.admin_user
  admin_password = var.admin_password
  admin_hash = var.admin_hash
  env = var.env
  domain = var.domain
  s3_endpoint = var.s3_endpoint
  s3_key = var.s3_key
  s3_secret = var.s3_secret

}