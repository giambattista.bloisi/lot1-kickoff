env = "local"
kube_context= "kind-local-dataplatform"
domain = "local-dataplatform"
admin_user = "admin"
admin_password = "admin"
admin_hash = "$2y$10$Wd.mnnrDG01KJ42aVtC89.FdXOvyRm4RNfDfZ5F8k4r/fmSZgrIEq" # generate with htpasswd -bnBC 10 "" <admin_password>
s3_endpoint = "https://minio.lot1-minio-tenant.svc.cluster.local"
s3_key= "minio"
s3_secret = "minio123"

/*
{
    "type": "s3",
    "settings": {
        "bucket": "opensearch-repo",
        "base_path": "lot1",
        "endpoint": "https://minio.lot1-minio-tenant.svc.cluster.local",
        "access_key": "minio",
        "secret_key": "minio123"
    }
}

*/