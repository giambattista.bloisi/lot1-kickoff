variable "env" {
  type    = string
  default = "local"
}

variable "kube_config" {
  type    = string
  default = "~/.kube/config"
}

variable "kube_context" {
  type    = string
  default = "default"
}

variable "namespace_prefix" {
  type    = string
  default = "lot1-"
}

variable "domain" {
  type    = string
  default = "local-dataplatform"
}

variable "admin_user" {
  type    = string
}

variable "admin_password" {
  type    = string
}

variable "admin_hash" {
  type    = string
}

variable "s3_endpoint" {
  default = "https://minio.lot1-minio-tenant.svc.cluster.local"
}
variable "s3_key" {
  default = "minio"
}
variable "s3_secret" {
  default = "minio123"
}
