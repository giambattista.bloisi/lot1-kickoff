resource "helm_release" "minio_tenant" {
  name              = "minio-tenant"
  chart             = "tenant"
  repository        = "https://operator.min.io/"
  create_namespace  = "true"
  namespace         = "${var.namespace_prefix}minio-tenant"
  dependency_update = "true"
  version           = "5.0.12"

  values = [
    file("./envs/${var.env}/minio-tenant.yaml")
  ]

  set {
    name = "ingress.api.host"
    value = "minio.${var.domain}"
  }

  set {
    name = "ingress.console.host"
    value = "console-minio.${var.domain}"
  }
}

/*
resource "kubernetes_manifest" "minio_ingress" {
  manifest = yamldecode(<<YAML
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-minio
  namespace: block-storage
  annotations:
    kubernetes.io/ingress.class: "nginx"
    ## Remove if using CA signed certificate
    nginx.ingress.kubernetes.io/proxy-ssl-verify: "off"
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/proxy-body-size: "0"
spec:
  ingressClassName: nginx
  tls:
  - hosts:
      - minio.${var.domain}
    secretName: nginx-tls
  rules:
  - host: minio.${var.domain}
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: minio
            port:
              number: 443
  YAML
  )
}*/
