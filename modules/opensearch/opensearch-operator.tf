resource "kubernetes_namespace" "opensearch_operator" {
  metadata {
    name = "${var.namespace_prefix}opensearch-operator"
  }
}

resource "kubernetes_namespace" "opensearch_cluster" {
  metadata {
    name = "${var.namespace_prefix}opensearch-cluster"
  }
}

resource "helm_release" "opensearch-operator" {
  depends_on = [kubernetes_namespace.opensearch_operator, kubernetes_namespace.opensearch_cluster]

  chart      = "opensearch-operator"
  name       = "opensearch-operator"
  namespace  = "${var.namespace_prefix}opensearch-operator"
  create_namespace = false
  repository = "https://opensearch-project.github.io/opensearch-k8s-operator/"
  version    = "2.5.1"

  set {
    name = "manager.watchNamespace"
    value = "${var.namespace_prefix}opensearch-cluster"
  }

  # You can provide a map of value using yamldecode. Don't forget to escape the last element after point in the name
  set {
    name = "manager\\.extraEnv"
    value = yamlencode({
      name = "SKIP_INIT_CONTAINER",
      value = "true"
    })
  }
}

