terraform {
  required_providers {
    bcrypt = {
      source = "viktorradnai/bcrypt"
      version = "0.1.2"
    }
  }
}

provider "bcrypt" {
}

provider "helm" {
  # Several Kubernetes authentication methods are possible: https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs#authentication
  kubernetes {
    config_path = pathexpand(var.kube_config)
    config_context = var.kube_context
  }
}

provider "kubernetes" {
  config_path = pathexpand(var.kube_config)
  config_context = var.kube_context
}