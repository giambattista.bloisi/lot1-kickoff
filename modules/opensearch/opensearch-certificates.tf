resource "kubernetes_manifest" "opensearch_issuer" {
  depends_on = [kubernetes_namespace.opensearch_cluster]

  manifest = yamldecode(<<YAML
    apiVersion: cert-manager.io/v1
    kind: Issuer
    metadata:
      name:  selfsigned-issuer
      namespace: "${var.namespace_prefix}opensearch-cluster"
    spec:
      selfSigned: {}
  YAML
  )
}

resource "kubernetes_manifest" "opensearch_ca_certificate" {
  depends_on = [kubernetes_namespace.opensearch_cluster]

  manifest = yamldecode(<<YAML
    apiVersion: cert-manager.io/v1
    kind: Certificate
    metadata:
      name: ca-certificate
      namespace: "${var.namespace_prefix}opensearch-cluster"
    spec:
      secretName: ca-cert
      duration: 9000h # ~1year
      renewBefore: 360h # 15d
      commonName: Test CA
      isCA: true
      privateKey:
        size: 2048
      usages:
        - digital signature
        - key encipherment
      issuerRef:
        name: selfsigned-issuer
  YAML
  )
}

resource "kubernetes_manifest" "opensearch_ca_issuer" {
  depends_on = [kubernetes_namespace.opensearch_cluster]

  manifest = yamldecode(<<YAML
    apiVersion: cert-manager.io/v1
    kind: Issuer
    metadata:
      name: ca-issuer
      namespace: "${var.namespace_prefix}opensearch-cluster"
    spec:
      ca:
        secretName: ca-cert
  YAML
  )
}

resource "kubernetes_manifest" "opensearch_cluster_certificate" {
  depends_on = [kubernetes_namespace.opensearch_cluster]

  manifest = yamldecode(<<YAML
    apiVersion: cert-manager.io/v1
    kind: Certificate
    metadata:
      name: opensearch-certs
      namespace: "${var.namespace_prefix}opensearch-cluster"
    spec:
      secretName: opensearch-certs
      duration: 9000h # ~1year
      renewBefore: 360h # 15d
      isCA: false
      privateKey:
        size: 2048
        algorithm: RSA
        encoding: PKCS8
      dnsNames:
        - opensearch-cluster.${var.domain}
        - opensearch-cluster
        - opensearch-cluster-masters-0
        - opensearch-cluster-masters-1
        - opensearch-cluster-masters-2
        - opensearch-cluster-bootstrap-0
      usages:
        - signing
        - key encipherment
        - server auth
        - client auth
      commonName: Opensearch_Node
      issuerRef:
        name: ca-issuer
  YAML
  )
}
resource "kubernetes_manifest" "opensearch_admin_certificate" {
  depends_on = [kubernetes_namespace.opensearch_cluster]

  manifest = yamldecode(<<YAML
    apiVersion: cert-manager.io/v1
    kind: Certificate
    metadata:
      name: opensearch-admin-certs
      namespace: "${var.namespace_prefix}opensearch-cluster"
    spec:
      secretName: opensearch-admin-certs
      duration: 9000h # ~1year
      renewBefore: 360h # 15d
      isCA: false
      privateKey:
        size: 2048
        algorithm: RSA
        encoding: PKCS8
      commonName: OpenSearch_Admin
      usages:
        - signing
        - key encipherment
        - server auth
        - client auth
      issuerRef:
        name: ca-issuer
  YAML
  )
}
resource "kubernetes_manifest" "opensearch_dashboard_certificate" {
  depends_on = [kubernetes_namespace.opensearch_cluster]

  manifest = yamldecode(<<YAML
    apiVersion: cert-manager.io/v1
    kind: Certificate
    metadata:
      name: opensearch-dashboards-certs
      namespace: "${var.namespace_prefix}opensearch-cluster"
    spec:
      secretName: opensearch-dashboards-certs
      duration: 9000h # ~1year
      renewBefore: 360h # 15d
      isCA: false
      privateKey:
        size: 2048
        algorithm: RSA
        encoding: PKCS8
      dnsNames:
        - opensearch-cluster-dashboards
      usages:
        - signing
        - key encipherment
        - server auth
        - client auth
      issuerRef:
        name: ca-issuer
  YAML
  )
}