variable "env" {
  type    = string
  default = "local"
}

variable "kube_config" {
  type    = string
  default = "~/.kube/config"
}

variable "kube_context" {
  type    = string
  default = "default"
}

variable "namespace_prefix" {
  type    = string
  default = "lot1-"
}

variable "domain" {
  type    = string
  default = "local-dataplatform"
}

variable "admin_user" {
  type    = string
}

variable "admin_password" {
  type    = string
}

variable "admin_hash" {
  type    = string
}

